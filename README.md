# Easy FireWall

This is a ufw-esque script that runs on a jailbroken IOS device. It uses the  built in packet filter to block or allow IP addresses. To install just clone this repo, then move the efw script onto your jailbroken IDevice. Ideally it should be placed somewhere in $PATH so it can be executed from the command line (like /usr/bin). EFW's syntax is really similar to debian's ufw.  

Usage: efw COMMAND


